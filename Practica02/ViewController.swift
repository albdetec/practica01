//
//  ViewController.swift
//  Practica02
//
//  Created by Alberto Téllez on 28/05/2019.
//  Copyright © 2019 albhyde. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var productsTableView: UITableView!
    var products = [Product] ()

    override func viewDidLoad() {
        
        products.append(Product(name: "Harina"))
        products.append(Product(name: "Leche"))
        products.append(Product(name: "Café"))
        products.append(Product(name: "Azúcar"))
        products.append(Product(name: "Arroz"))
        
        productsTableView.register(UITableViewCell.self, forCellReuseIdentifier: "tablecellBasic")
        
        
        super.viewDidLoad()
        
    }


}

extension ViewController: UITableViewDataSource  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return products.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let product = products[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "tablecellBasic", for: indexPath)
        
        cell.textLabel?.text = product.name
        
        return cell
        
    
        
    }
}

